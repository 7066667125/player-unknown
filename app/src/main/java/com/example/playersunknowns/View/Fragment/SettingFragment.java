package com.example.playersunknowns.View.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.playersunknowns.Adapter.SettingMenuAdapter;
import com.example.playersunknowns.R;
import com.example.playersunknowns.View.Pojo.User;

import java.util.ArrayList;
import java.util.List;

/**
 * This method is used to handle SettingFragment functionality.
 */
public class SettingFragment extends Fragment {
    View v;
    private RecyclerView mRecyclerView;
    private List<User> mFirstuser;

    public SettingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_setting, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerview_setting_menu);
        SettingMenuAdapter recyclerAdapter = new SettingMenuAdapter(getContext(), mFirstuser);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(recyclerAdapter);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirstuser = new ArrayList<>();
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
        mFirstuser.add(new User("Ankit", "(+91)9595868622", R.drawable.icon));
    }
}
